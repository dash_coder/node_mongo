var express = require('express');
var app = express();
var signUp = require('./Controller/signUp.js');
var product = require('./Controller/product.js');
var category= require('./Controller/categories.js');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path =require('path');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/',function(req,res)
{
	res.render('ankit.jade');
})

app.post('/signUp',signUp.register);
app.post('/login',signUp.login);
app.get('/user',signUp.showAllUser);
app.post('/updateData',signUp.updateData);
app.post('/registerAdmin',signUp.registerAdmin);
// products
app.post('/addProduct',product.addProduct);
app.post('/deleteProduct',product.deleteProduct);

// app.get('/user/:email',signUp.showAllUserByEmail);

//category 
app.post('/addCategory',category.addCategory);
app.post('/deleteCategory',category.deleteCategory);
app.post('/showAllCategory',category.showAllCategory);
app.post('/updateCategory',category.updateCategory);
app.post('/showCategory',category.showCategory);




var server = require("http").createServer(app);
// Start server
server.listen(8083, function() {
	server.timeout = 9000000;
	console.log("Express server listening on 8083 mode");
});