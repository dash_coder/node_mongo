var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var category = new Schema({
    name: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: Date,
    created_by: Number,
    is_deleted: Boolean,
    deleted_by: Number,
    image:{type:String},
    icon:{type:String},
    subcategory: {type: Schema.ObjectId, ref: 'product'}
    

});

module.exports = mongoose.model('categories', category);
