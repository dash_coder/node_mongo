var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Schema = mongoose.Schema;

var admin=  new Schema({
    name: {type: String},
    email:{type:String},
    password:{type:String},
    accessToken:{type:String},
    created_at : { type:Date , default:Date.now},
    updated_at:Date
});


module.exports = mongoose.model('admin', admin);
