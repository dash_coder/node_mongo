var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Schema = mongoose.Schema;

var userSchema = new Schema({
  name: String,
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  email:{type:String,required:true},
  location: String,
  created_at: Date,
  updated_at: Date
});


var user = mongoose.model('user', userSchema);
module.exports = user;