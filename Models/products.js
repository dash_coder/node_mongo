var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Schema = mongoose.Schema;

var product = new Schema({
    productName: {
        type: String
    },
    description: {
        type: String
    },
    image: {
        type: String
    },
    icon: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },

    updated_at: Date,
    logitude: String,
    latitude: String,
    price: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    dealLocation: {
        type: String
    },
    offerValidFrom: {
        type: String
    },
    code: {
        type: String
    },
    likes: String,
    is_deleted: {
        type: Boolean,
        default: false
    }
});


module.exports = mongoose.model('product', product);