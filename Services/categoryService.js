'use strict';

var Models = require('../Models/categories');

//Get Users from DB
var getCategory = function (criteria, projection, options, callback) {
    Models.find(criteria, projection, options, callback);
};

//Insert User in DB
var createCategory = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};

//Update User in DB
var updateCategory = function (criteria, dataToSet, options, callback) {
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};

var getCategoryById = function(criteria,populateArr,callback)
{
	Models.find(criteria,{},{lean:true}).populate('subcategory').exec(callback);
}


module.exports = {
    getCategory: getCategory,
    createCategory: createCategory,
    updateCategory: updateCategory,
    getCategoryById:getCategoryById
};

