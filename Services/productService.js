'use strict';

var Models = require('../Models/products');

//Get Users from DB
var getProduct = function (criteria, projection, options, callback) {
    Models.find(criteria, projection, options, callback);
};

//Insert User in DB
var createProduct = function (objToSave, callback) {
    new Models(objToSave).save(callback)
};

//Update User in DB
var updateProduct = function (criteria, dataToSet, options, callback) {
    Models.findOneAndUpdate(criteria, dataToSet, options, callback);
};

module.exports = {
    getProduct: getProduct,
    createProduct: createProduct,
    updateProduct: updateProduct
};

