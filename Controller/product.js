var async = require('async');
var mongoose = require('mongoose');
var sendResponse = require('./errorResponse');
var connection = require('./connection')
var productService = require('../Services/productService');
var categoryService = require('../Services/categoryService');
var adminService = require('../Services/adminService');

exports.addProduct = function(req, res) {

    var productName;
    var accessToken;
    var userId;
    var image;
    var description;
    var price;
    var phone;
    var dealLocation;
    var offerValidFrom;
    var code;
    var categoryId;
    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);
            if (!req.body.productName) {
                var msg = "please enter product name";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.accessToken) {
                var msg = "invalid accessToken";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.image) {
                var msg = "invalid image url";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.description) {
                var msg = "enter description";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.price) {
                var msg = "enter price value";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.phone) {
                var msg = "enter phoe number";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.dealLocation) {
                var msg = "enter dealLocation";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.offerValidFrom) {
                var msg = "enter offer valid from";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.code) {
                var msg = "enter code";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.categoryId) {
                var msg = "enter categoryId";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }


            productName = req.body.productName;
            accessToken = req.body.accessToken;
            image = req.body.image;
            description = req.body.description;
            price = req.body.price;
            phone = req.body.phone;
            dealLocation = req.body.dealLocation;
            offerValidFrom = req.body.offerValidFrom;
            code = req.body.code;
            categoryId = req.body.categoryId;
            callback(null);

        },

        getUserId: ['checkBlank',
            function(result, callback) {
                var criteria = {
                    accessToken: accessToken
                }
                adminService.getAdmin(criteria, {}, {
                    lean: true
                }, function(err, result) {
                    if (err)
                        callback(err)
                    else {
                        if (result != null) {
                            userId = result._id;
                            callback(null);
                        } else {
                            var msg = "unauthorized accessToken";
                            return sendResponse.sendErrorMessage(msg, res, 401);
                        }
                    }

                })
            }
        ],
        insert: ['getUserId',
            function(result, callback) {
                var obj = {
                    productName: productName,
                    updated_at: new Date(),
                    created_by: userId,
                    image: image,
                    description: description,
                    price: price,
                    phone: phone,
                    dealLocation: dealLocation,
                    offerValidFrom: offerValidFrom,
                    code: code
                }
                productService.createProduct(obj, function(err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        console.log("result category saved....", result);
                        var criteria = {
                            _id: categoryId
                        }
                        var projection = {
                            $addToSet: {
                                subcategory: result._id
                            }
                        }
                        categoryService.updateCategory(criteria, projection, {}, function(err, result) {
                            if (err)
                                callback(err);
                            else {
                                console.log("update category...",result);
                                callback(null);
                            }
                        })

                    }
                })

            }
        ]
    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "product added successfully";
            return sendResponse.sendSuccessData({}, msg, res, 200);

        }
    })



}



exports.deleteProduct = function(req, res) {
    var accessToken;
    var productId;

    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);

            if (!req.body.productId) {
                var msg = "please enter productId";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.accessToken) {
                var msg = "please enter accessToken";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }

            productId = req.body.productId;
            accessToken = req.body.accessToken;
            callback(null);

        },
        checkDb: ['checkBlank',
            function(result, callback) {
                var criteria = {
                    accessToken: accessToken
                }
                adminService.getAdmin(criteria, {}, {}, function(err, result) {
                    if (err)
                        callback(err)
                    else {
                        if (result != null) {
                            userId = result._id;
                            callback(null);
                        } else {
                            var msg = "unauthorized accessToken";
                            return sendResponse.sendErrorMessage(msg, res, 401);
                        }
                        callback(null);
                    }

                })


            }
        ],
        deleteProduct: ['checkDb',
            function(result, callback) {
                var obj = {
                    $set: {
                        is_deleted: true,
                        deleted_by: userId
                    }
                }
                productService.updateProduct({
                    _id: productId
                }, obj, {
                    lean: true
                }, function(err, result) {
                    if (err) callback(err);
                    else {
                        console.log('done');
                        callback(null);

                    }

                })

            }
        ]


    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "category deleted successfully";
            return sendResponse.sendSuccessData({}, msg, res, 200);
        }
    })


}