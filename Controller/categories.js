var async = require('async');
var mongoose = require('mongoose');
var sendResponse = require('./errorResponse');
var connection = require('./connection')
var categoryService = require('../Services/categoryService');
var adminService = require('../Services/adminService');

exports.addCategory = function(req, res) {

    var name;
    var accessToken;
    var userId;

    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);
            if (!req.body.categoryName) {
                var msg = "please enter name";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.accessToken) {
                var msg = "invalid accessToken";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }


            name = req.body.categoryName;
            accessToken = req.body.accessToken;
            callback(null);

        },

        getUserId: ['checkBlank',
            function(result, callback) {
                var criteria = {
                    accessToken: accessToken
                }
                adminService.getAdmin(criteria, {}, {}, function(err, result) {
                    if (err)
                        callback(err)
                    else {
                        if (result != null) {
                            console.log("..............",result)
                            userId = result._id;
                            callback(null);
                        } else {
                            var msg = "unauthorized accessToken";
                            return sendResponse.sendErrorMessage(msg, res, 401);


                        }
                    }

                })

            }
        ],
        checkUser: ['checkBlank',
            function(result, callback) {
                var criteria = {
                    name: name
                }
                categoryService.getCategory(criteria, {}, {}, function(err, result) {
                    if (err)
                        callback(err);
                    else {
                        console.log(result);
                        if (result != null && result.length>0) {
                            var msg = "category already exist";
                            return sendResponse.sendErrorMessage(msg, res, 401);
                        } else
                            callback(null);
                    }

                })

            }
        ],
        insert: ['checkUser', 'getUserId',
            function(result, callback) {
                // console.log(callback)
                var obj = {
                    name: name,
                    updated_at: new Date(),
                    created_by: userId
                }
                categoryService.createCategory(obj, function(err, result) {
                    if (err) {
                        callback(err);
                    } else {
                        console.log("category saved");
                        callback(null);
                    }

                })
            }
        ]
    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "category added successfully";
            return sendResponse.sendSuccessData({}, msg, res, 200);

        }
    })



}



exports.deleteCategory = function(req, res) {
    var accessToken;
    var cateagoryId;

    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);

            if (!req.body.categoryId) {
                var msg = "please enter categoryId";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.accessToken) {
                var msg = "please enter accessToken";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }

            categoryId = req.body.categoryId;
            accessToken = req.body.accessToken;
            callback(null);

        },
        checkDb: ['checkBlank',
            function(result, callback) {
                var criteria = {
                    accessToken: accessToken
                }
                adminService.getAdmin(criteria, {}, {}, function(err, result) {
                    if (err)
                        callback(err)
                    else {
                        if (result != null) {

                            callback(null);
                        } else {
                            var msg = "unauthorized accessToken";
                            return sendResponse.sendErrorMessage(msg, res, 401);


                        }
                    }

                })
            }
        ],
        deleteCategory: ['checkDb',
            function(result, callback) {
                var criteria = {
                    _id: categoryId
                }
                var dataTosent = {
                    $set: {
                        is_deleted: true,
                        deleted_by: userId
                    }
                }
                categoryService.updateCategory(criteria, dataTosent, {}, function(err, result) {
                    if (err) callback(err);
                    else {
                        console.log('done');
                        callback(null);

                    }

                });
            }
        ]


    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "category deleted successfully";
            return sendResponse.sendSuccessData({}, msg, res, 200);
        }
    })


}

exports.showAllCategory = function(req, res) {
    var arr = [];
    categoryService.getCategory({}, {}, {lean:true}, function(err, result) {
        if (err) {
            console.log(err);
            var msg = "something went wrong";
            return sendResponse.sendErrorMessage(msg, res, 400);
        } else {
            // console.log(result.length)
            /*for (var i = 0; i < result.length; i++) {
                (function(i) {
                    // console.log(result[i]);
                    arr.push(result[i].name);

                    if (i == result.length - 1) {
                        console.log(arr)
                        var msg = "welcome.......";
                        return sendResponse.sendSuccessData(arr, msg, res, 200);

                    }
                })(i);

            }*/

            return sendResponse.sendSuccessData(result, msg, res, 200);
        }
    })
}

exports.updateCategory = function(req, res) {
    var accessToken;
    var cateagoryId;
    var categoryName;


    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);

            if (!req.body.categoryId) {
                var msg = "please enter categoryId";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.accessToken) {
                var msg = "please enter accessToken";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.categoryName) {
                var msg = "please enter your name";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }

            categoryId = req.body.categoryId;
            accessToken = req.body.accessToken;
            categoryName = req.body.categoryName
            callback(null);

        },
        checkDb: ['checkBlank',
            function(result, callback) {
                var criteria = {
                    accessToken: accessToken
                }
                adminService.getAdmin(criteria, {}, {}, function(err, result) {
                    if (err)
                        callback(err)
                    else {
                        if (result != null) {

                            callback(null);
                        } else {
                            var msg = "unauthorized accessToken";
                            return sendResponse.sendErrorMessage(msg, res, 401);


                        }
                    }

                })
            }
        ],
        updateCategory: ['checkDb',
            function(result, callback) {
                var criteria = {
                    _id: categoryId
                }
                var dataTosent = {
                    $set: {
                        name: categoryName,
                        updated_at: new Date()
                    }
                }
                categoryService.updateCategory(criteria, dataTosent, {}, function(err, result) {
                    if (err) callback(err);
                    else {
                        console.log('done');
                        callback(null);

                    }

                });
            }
        ]


    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "category updated successfully";
            return sendResponse.sendSuccessData({}, msg, res, 200);
        }
    })


}


exports.showCategory = function(req, res) {
    var arr = [];
    var categoryId;
    async.waterfall([

            function(callback) {
                if (!req.body.categoryId) {
                    var msg = "please enter name";
                    return sendResponse.sendErrorMessage(msg, res, 400);
                }
                categoryId = req.body.categoryId;
                callback(null);
            },
            function(callback) {
                var populateModel = [
                {
                    path: "products",
                    match: {},
                    select: 'subcategory',
                    options: {lean: true}
                }
            ]

                categoryService.getCategoryById({
                        _id: categoryId
                    },populateModel, function(err, result) {
                        if (err) {
                            console.log(err);
                            var msg = "something went wrong";
                            return sendResponse.sendErrorMessage(msg, res, 400);
                        } else {
                            return sendResponse.sendSuccessData(result, "", res, 200);

                            // console.log(result.length)
                            /*for (var i = 0; i < result.length; i++) {
                (function(i) {
                    // console.log(result[i]);
                    arr.push(result[i].name);

                    if (i == result.length - 1) {
                        console.log(arr)
                        var msg = "welcome.......";
                        return sendResponse.sendSuccessData(arr, msg, res, 200);

                    }
                })(i);*/

                        }

                    })

        }


    ],
    function(err, result) {
        if (err)
            console.log(err)
        else
            console.log(result)
    })


}