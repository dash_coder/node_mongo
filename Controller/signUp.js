var async = require('async');
var mongoose = require('mongoose');
var sendResponse = require('./errorResponse');
var connection = require('./connection')
var userModel = require('../Models/customer');
var admin = require('../Models/admin');
var adminService = require('../Services/adminService')
var jwt = require('jsonwebtoken');

exports.register = function(req, res) {

    var name;
    var email;
    var password;
    var username;


    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);
            if (!req.body.name) {
                var msg = "please enter name";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.email) {
                var msg = "please enter email";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.password) {
                var msg = "please enter password";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.username) {
                var msg = "please enter username";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }


            name = req.body.name;
            email = req.body.email;
            password = req.body.password;
            username=req.body.username;

            callback(null);

        },
        checkUser: ['checkBlank',
            function(result, callback) {
                userModel.findOne({
                    email: email
                }, function(err, result) {
                    if (err)
                        callback(err);
                    else {
                        console.log(result);
                        if (result != null) {
                            var msg = "user already exist";
                            return sendResponse.sendErrorMessage(msg, res, 401);
                        } else
                            callback(null);
                    }
                })

            }
        ],

        insert: ['checkUser',
            function(result, callback) {
                console.log(callback)
                var userCreated = new userModel({
                    name: name,
                    email: email,
                    password: password,
                    username:username,

                    updated_at: new Date()
                });

                //save model to MongoDB
                userCreated.save(function(err) {
                    if (err) {
                        callback(err);
                    } else {
                        console.log("Post saved");
                        callback(null);
                    }
                });


            }
        ]
    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "registered....";
            return sendResponse.sendSuccessData({}, msg, res, 200);

        }
    })



}



exports.login = function(req, res) {
    var email;
    var password;

    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);

            if (!req.body.email) {
                var msg = "please enter email";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.password) {
                var msg = "please enter password";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }

            email = req.body.email;
            password = req.body.password;
            callback(null);

        },
        checkDb: ['checkBlank',
            function(result, callback) {
                userModel.findOne({
                    email: email
                }, function(err, result) {
                    if (err)
                        callback(err);
                    else {

                        userModel.findOne({
                            password: password
                        }, function(err, result) {
                            if (err)
                                callback(err);
                            else {
                                if (result != null)
                                    callback(null);
                                else {
                                    var msg = "password or email wrong";

                                    return sendResponse.sendErrorMessage(msg, res, 401);


                                }
                            }
                        })
                    }
                })


            }
        ]


    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "welcome.......";
            return sendResponse.sendSuccessData({}, msg, res, 200);
        }
    })


}

exports.showAllUser = function(req, res) {
    var arr = [];
    userModel.find({}, function(err, result) {
        if (err) {
            console.log(err);
            var msg = "something went wrong";
            return sendResponse.sendErrorMessage(msg, res, 400);
        } else {
            // console.log(result.length)
            for (var i = 0; i < result.length; i++) {
                (function(i) {
                    // console.log(result[i]);
                    arr.push(result[i].name);

                    if (i == result.length - 1) {
                        console.log(arr)
                        var msg = "welcome.......";
                        return sendResponse.sendSuccessData(arr, msg, res, 200);

                    }
                })(i);

            }

        }
    })
}


exports.updateData = function(req, res) {
    var email = req.body.email;
    userModel.update({
        email: email
    }, {
        $set: {
            name: req.body.name,
            password: req.body.password
        }
    }, function(err, result) {
        if (err)
            console.log('err.......', err);
        else {
            res.send(result);
        }
    });

}

exports.showAllUserByEmail = function(req, res) {
    if (req.params.email) {
        userModel.find({
            email: req.params.email
        }, function(err, result) {
            if (err) {
                console.log(err);
                var msg = "something went wrong";
                return sendResponse.sendErrorMessage(msg, res, 400);
            } else {
                var msg = "";
                return sendResponse.sendSuccessData(result, msg, res, 200);

            }
        })
    }
}

exports.registerAdmin = function(req, res) {

    var name;
    var email;
    var password;
    var accessToken;


    async.auto({
        checkBlank: function(callback) {
            // console.log(callback);
            if (!req.body.name) {
                var msg = "please enter name";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.email) {
                var msg = "please enter email";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            if (!req.body.password) {
                var msg = "please enter password";
                return sendResponse.sendErrorMessage(msg, res, 400);
            }
            

            name = req.body.name;
            email = req.body.email;
            password = req.body.password;
           
            callback(null);

        },
        checkUser: ['checkBlank',
            function(result, callback) {
                var criteria ={
                    email: email
                }
                adminService.getAdmin(criteria,{},{lean:true},function(err,result)
                {
                    console.log(result.length)
                   if (result != null && result.length>0) {
                            var msg = "user already exist";
                            return sendResponse.sendErrorMessage(msg, res, 401);
                        } else
                            callback(null);
                    } 
                )
            }    

            ],
            genrateAccessToken:['checkUser',function(result,callback)
            {
                accessToken="123456789";
                callback(null);


            }],

        insert: ['genrateAccessToken',
            function(result, callback) {
                // console.log(callback)
                var obj ={
                    name: name,
                    email: email,
                    password: password,
                    accessToken:accessToken,
                    updated_at: new Date()
                }
                adminService.createAdmin(obj,function(err,result)
                {
                    if (err) {
                        callback(err);
                    } else {
                        console.log("Post saved");
                        callback(null);
                    }

                })
                


            }
        ]
    }, function(err, result) {
        if (err)
            console.log(err)
        else {
            var msg = "admin registered....";
            return sendResponse.sendSuccessData({}, msg, res, 200);

        }
    })



}

